package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.example.demo.Model.Customer;

@Service
public class CustomerService {
    Customer customer1 = new Customer(1, "Linh Giang", 10);
    Customer customer2 = new Customer(2, "Châu Giang", 20);
    Customer customer3 = new Customer(3, "Trường Giang", 30);

    public ArrayList<Customer> gCustomerList(){
        ArrayList<Customer> customerList = new ArrayList<>();

        customerList.add(customer1);
        customerList.add(customer2);
        customerList.add(customer3);

        return customerList ;
        
    }

}
