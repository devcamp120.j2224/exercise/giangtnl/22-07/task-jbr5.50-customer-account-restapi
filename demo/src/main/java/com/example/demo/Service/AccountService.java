package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Account;

@Service
public class AccountService {
    @Autowired
    private CustomerService CustomerService ;
    Account account1 = new Account(1, 12000);
    Account account2 = new Account(2, 22000);
    Account account3 = new Account(3, 32000);

    public ArrayList<Account> getAccountList(){
        ArrayList<Account> allAccount = new ArrayList<>();
        account1.setCustomer(CustomerService.customer1);
        account2.setCustomer(CustomerService.customer2);
        account3.setCustomer(CustomerService.customer3);

        allAccount.add(account1);
        allAccount.add(account2);
        allAccount.add(account3);

        return allAccount ;
    }

}
