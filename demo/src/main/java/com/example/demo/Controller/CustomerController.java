package com.example.demo.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Customer;
import com.example.demo.Service.CustomerService;

@RestController
public class CustomerController {
    
        @Autowired
        private CustomerService customerService;

        @GetMapping("/customers")
        public ArrayList<Customer> getCustomer(){
            ArrayList<Customer> allCus = customerService.gCustomerList();
            return allCus ;
        }
    
}
